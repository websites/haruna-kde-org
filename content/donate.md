---
title: Donate
layout: donate
menu:
  main:
    weight: 5
---
## Haruna
[GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)

## KDE
https://kde.org/donate
