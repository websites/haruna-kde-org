---
layout: get-involved
title: Get Involved
name: Haruna
userbase: Haruna
menu:
  main:
    weight: 4
---

Want to contribute to Haruna? Check out the [open bugs](https://bugs.kde.org/buglist.cgi?component=generic&list_id=1913178&product=Haruna&resolution=---) and the [source code](https://invent.kde.org/multimedia/haruna).
