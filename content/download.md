---
title: Download
layout: download
appstream: org.kde.haruna
name: Haruna
gear: false
flatpak: true
menu:
  main:
    weight: 2
---
