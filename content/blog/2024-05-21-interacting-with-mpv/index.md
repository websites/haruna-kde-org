---
title: Interacting with mpv
description: How to use mpv scripts and config files
author: George Florea Bănuș
date: 2024-05-21T12:00:00+02:00
---

Haruna allows you to run any [mpv command](https://mpv.io/manual/stable/#list-of-input-commands) through it's `Custom Commands` settings page.
The most useful command would be the [set](https://mpv.io/manual/stable/#command-interface-set-%3Cname%3E-%3Cvalue%3E) command which lets you set an mpv property to some value. For example to set the hwdec property to vaapi you use `set hwdec vaapi` in the command field of Haruna's `Custom Commands` page; if the value contains spaces wrap it in quotes.

Custom commands can be triggered either at startup or through a shortcut (this can be set when creating the custom command).

If you want to set multiple commands you can create an mpv config file and load it with `set include "/path/to/config/file.conf"`.

----

mpv scripts can also be loaded, but they are more cumbersome to use. Create a custom command loading your script `load-script "/path/to/script.lua"`.

If you want to interact with a script you must create another custom command `script-message-to target arg1 arg2 ...` [script-message-to](https://mpv.io/manual/stable/#command-interface-script-message-to).

* `target` - the filename of the script (without the file extension)
* `arg1` - the name assigned to a function inside your script by [register_script_message](https://mpv.io/manual/stable/#lua-scripting-mp-register-script-message(name,-fn)), can be the same as the function name
* `arg2`, `arg3` etc. - arguments passed to the function

Example:

```lua
-- my_simple_script.lua
function set_volume(volume)
    mp.commandv("set", "volume", volume)
end

mp.register_script_message("set_volume", set_volume)
-- ............................^ name to use in the script-message-to call

-- mp.register_script_message("set_volume", set_volume)
-- .............................................^ name of the function

```

1. create a my_simple_script.lua containing the code above
1. create a custom command to load the script `load-script "/path/to/my_simple_script.lua"`
1. create a custom command (triggered by a shortcut) to interact with the script `script-message-to my_simple_script set_volume 56`
1. assign a shortcut to the `script-message-to` custom command
1. trigger the `script-message-to` command after the script is loaded (depends on how you load the script, at startup or by shortcut)

----

#### Note
_When running the flatpak version the scripts won't be able to access system programs/binaries/executables._
