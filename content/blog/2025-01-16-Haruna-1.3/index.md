---
title: Haruna 1.3
description: Haruna 1.3
author: George Florea Bănuș
date: 2025-01-16T13:13:13+02:00
---

Haruna version 1.3.3 is out.

There have been lots of code refactoring, so make sure to report any bugs you encounter. There's a "Report bug" menu entry under the "Help" menu or in settings in the about page.

The default actions for left and right mouse buttons have changed: left click is now play/pause and right click opens the context menu. Actions can be changed in settings in the Mouse page.

Another setting whose default changed is the `Start playing` setting which is now on by default.

`Start playing` controls the playback state, playing or paused, when restoring the position of the file.

----

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="70"></a>

Windows version:
 - <a href="https://download.kde.org/stable/haruna/1.3.3/haruna-1.3.3-windows-gcc-x86_64.exe">haruna-1.3.3-windows-gcc-x86_64.exe</a>
 - <a href="https://download.kde.org/stable/haruna/1.3.3/haruna-1.3.3-windows-gcc-x86_64.7z">haruna-1.3.3-windows-gcc-x86_64.7z</a>

Availability of other package formats depends on your distro and the people who package Haruna.

**If you like Haruna then support its development: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)**

Feature requests and bugs should be posted on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), ignoring the bug report template can result in your report being ignored.

----

### Changelog
#### 1.3.3
##### Bugfixes
*   Fixed app not getting focused when opening a file while single instance is enabled and player is open
*   Fixed overlapping the window title with the duration info in the footer
*   Implemented screen inhibition on Windows

#### 1.3.2
##### Bugfixes
*   Fixed shortcuts not working with non english system language
*   Fixed restoring playback position

#### 1.3.1
##### Bugfixes
*   Fixed screen inhibition not working
*   Fixed setting volume through mpris2

#### 1.3.0
##### Features
*   Changed the settings related to restoring the playback position, if you changed the defaults you might have to redo them
*   Playlist: added context menu entries for non local urls to be opened in the browser and to be removed (from the playlist)
*   Added setting (in General) to start app in fullscreen
*   Added setting (in Subtitles) to control subtitle auto selection
*   Changed left and right mouse button default actions: left click is now play/pause and right click opens the context menu. Actions can be changed in settings Mouse page
*   Dragging the seek/progress bar will update the main view as well as the seekbar preview
*   Decreased the minimum window size
*   Replaced hardware decoding checkbox with an option in the hardware decoding combobox/drop-down

##### Bugfixes
*   Improved scrolling performance of playlist, shortcuts and seekbar's chapters menu
*   Fixed issue with setting shortcuts with modifier keys (ctrl, alt etc.)
*   Fixed active audio and subtitle tracks not being correctly checked
*   Removed empty header toolbar from settings window
*   Fixed a bug where the app would freeze when loading a file with lots of tracks
*   Improved menubar animation
*   The floating footer now hides when the mouse leaves the app
