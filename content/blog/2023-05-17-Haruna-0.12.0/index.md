---
title: Haruna 0.12.0
description: Haruna 0.12.0
author: George Florea Bănuș
date: 2023-08-25T12:00:00+02:00
---
<style>
.shortcut {
  margin-right: 10px;
  font-weight: bold;
}
</style>


Haruna version 0.12.0 is out. You can get it now on flathub:

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="90"></a>

Availability of other package formats depends on your distro and the people who package Haruna.

There is a Windows version <a href="https://binary-factory.kde.org/view/Windows%20MingW/job/Haruna_Nightly_mingw64/" target="_blank">here</a>.

Consider making a donation if you like Haruna: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)

Issues should be reported on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but make sure to fill in the template and provide as much info as possible.

----

### Changelog:
#### 0.12.0
Features:
 * Added preview thumbnail to the progress bar, can be configured in the general settings page

    <a href="preview.png"><img src="preview-thumb.png" alt="preview feature showcase"></a>

 * Added setting to allow only a single instance, can be configured in the general settings page
 * Added action selection popup where you can search all available actions and trigger them (open
   with <span class="shortcut">Ctrl+`</span> (backtick), similar to KCommandBar)

    <a href="actions-popup.png"><img src="actions-popup-thumb.png" alt="actions popup feature showcase"></a>

 * Added setting to auto resize window to the video resolution, on Wayland the maximum size is not
   constrained
 * Added setting  to hide playlist toolbar
 * Added menu item for each settings page under settings menu
 * The screenshot path is shown in the osd when using mpv 0.36.0 or later versions

    <a href="screenshot-path.png"><img src="screenshot-path-thumb.png" alt="screenshot path feature showcase"></a>

 * Added a "Scroll to playing item" entry to the playlist context menu
 * Added a "Open url" entry to the playlist context menu for online url
 * Hiding/showing the menubar, top toolbar and bottom toolbar is now animated
 * Added track selection menu entries to the hamburger menu

    <a href="haburgermenu.png"><img src="haburgermenu-thumb.png" alt="haburgermenu feature showcase"></a>


Bugfixes:
 * Fixed not blocking inhibition (turning the display off, sleep etc.) during playback
