---
title: Haruna 1.1.1 (updated)
description: Haruna 1.1.1
author: George Florea Bănuș
date: 2024-05-21T12:00:00+02:00
---
_Update: Windows versions removed as they are broken._

----

Haruna version 1.1.1 is out.

You can get it now on flathub:

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="70"></a>

Availability of other package formats depends on your distro and the people who package Haruna.

**If you like Haruna then support its development: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)**

Feature requests and bugs should be posted on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but for bugs make sure to fill in the template and provide as much information as possible.

----

### Changelog:
#### 1.1.1
Bugfixes:
 * Subtitles menu not including manually added subtitles
 * Duration in the playlist being 0
 * Preview popup being visible for audio files
 * AB loop not resetting

#### 1.1.0
Features:
 * Setting to open playlist items with single click</li>

Bugfixes:
 * Video opening in a separate window when using mpv version 0.38.0
 * Mute resetting when opening a new file
 * Playing item in playlist is no longer bold, it made text hard to read under certain conditions
