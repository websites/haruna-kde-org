---
title: Haruna 1.1.2
description: Haruna 1.1.2
author: George Florea Bănuș
date: 2024-06-04T05:00:00+02:00
---

Haruna version 1.1.2 is out.

You can get it now on flathub:

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="70"></a>

Availability of other package formats depends on your distro and the people who package Haruna.

Windows version:
 - <a href="https://download.kde.org/stable/haruna/1.1.2/haruna-1.1.2-windows-gcc-x86_64.exe">haruna-1.1.2-windows-gcc-x86_64.exe</a>
 - <a href="https://download.kde.org/stable/haruna/1.1.2/haruna-1.1.2-windows-gcc-x86_64.7z">haruna-1.1.2-windows-gcc-x86_64.7z</a>
 - <a href="https://download.kde.org/stable/haruna/1.1.2/haruna-1.1.2-windows-gcc-x86_64-dbg.7z">haruna-1.1.2-windows-gcc-x86_64-dbg.7z</a>

**If you like Haruna then support its development: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)**

Feature requests and bugs should be posted on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but for bugs make sure to fill in the template and provide as much information as possible.

----

### Changelog:
#### 1.1.2
Bugfixes:
 * Disabled track selection menus and buttons when there are no tracks to be selected
 * Fixed custom command toggling
 * Re-added "Scroll to playing item" and "Trash file" options to playlist context menu, lost during Qt6 port
 * Fixed some mpv properties not being correctly set at startup
 * Fixed video rendering on Windows
