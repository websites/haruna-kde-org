---
title: Haruna 1.0.2
description: Haruna 1.0.2
author: George Florea Bănuș
date: 2024-03-27T13:00:00+02:00
---
<style>

.shortcut {
  margin-right: 10px;
  font-weight: bold;
}
</style>

Haruna version 1.0.2 is out.

There are not a lot of changes in this release as the focus was on porting to Qt6 and KF6 and code refactoring.
Some hwdec options have been removed, if needed they can be set in the settings under "Custom commands" as `set hwdec decoding_method_name` and choose "Run at startup".

You can get it now on flathub:

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="90"></a>

Windows version can be found <a href="https://cdn.kde.org/ci-builds/multimedia/haruna/master/windows/" target="_blank">here</a>.
Availability of other package formats depends on your distro and the people who package Haruna.

**If you like Haruna then support its development: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)**

Feature requests and bugs should be posted on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but for bugs make sure to fill in the template and provide as much information as possible.

----

### Changelog:
#### 1.0.2
Features:
 * Opening items from the playlist is faster
 * If `Maximum recent files` setting is set to zero the recent files are removed from the config file

Bugfixes:
 * Opening file through `Open File` action was not playing the file
 * Opening playlist file from playlist header was not doing anything
 * Hiding/showing Playlist toolbar setting was not working
 * Track sub-menus in Audio and Subtiles global menus being empty
 * Freeze when opening HamburgerMenu
