---
title: Haruna 0.10.3
description: Haruna 0.10.3
author: George Florea Bănuș
date: 2023-01-29T01:00:00+02:00
---

Haruna version 0.10.3 is out. You can get it now on flathub:

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="90"></a>

Availability of other package formats depends on your distro and the people who package Haruna.

💰 If you use the app and want it to improve make a donation: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)

Issues should be reported on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but make sure to fill in the template and provide as much info as possible.

----

### Changelog:
#### 0.10.0
- Playlist:
  - added options to, open m3u files, add files and urls, sort, clear and save the playlist
  - added context menu for the playlist items
  - added setting to remember playlist state
  - removed rowHeight setting
  - fixed "Increase font size when fullscreen" setting not working
- Recent files: fixed ordering, showing duplicates, clearing and adding when list is full
- Added command line option to override the ytdl format selection setting (this option is overwritten if the GUI setting is changed)
- Added option to set a default cover for music files
- Added MPRIS thumbnail
- Added setting for subtitles to be rendered in black borders
- Fixed OSD showing without text

#### 0.10.1
- Fixed not auto playing next file in playlist, when current file ends
- Fixed opened file not playing when "Auto load videos from same folder" setting is disabled

#### 0.10.2
- Fixed crash when "Maximum recent files" setting was set to zero

#### 0.10.3
- Fixed last played file not being remembered sometimes
- Fixed durations longer than 24 hours not displaying correctly and messing up with chapter labels
- Chapters menu has been rewritten to be more responsive with lots of chapters
- Fixed opening relative paths from a playlist file
- Fixed opening files containig # characters
