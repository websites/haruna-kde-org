---
title: Testing for the next release
description: Help test Haruna before the next release
author: George Florea Bănuș
date: 2022-12-22T01:00:00+02:00
---

Next release will come soon, but since there have been some big changes it would be nice to have them tested by more people.

The biggest changes are to the playlist which can now open m3u files, supports adding both local files and urls, can be sorted, cleared and saved.

The other big changes are to the recent files, which has been rewriten to fix a bunch of bugs, see below.

__To test you can use the flatpak beta__
```bash
flatpak remote-add flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
flatpak install flathub-beta org.kde.haruna
flatpak run --branch=beta org.kde.haruna
```
__or build it yourself__
```bash
git clone https://invent.kde.org/multimedia/haruna.git
cd haruna
# append `-D CMAKE_INSTALL_PREFIX:PATH=/your/custom/path` to install to a custom location
cmake -B build -G Ninja
cmake --build build
cmake --install build
```

### Full list of changes:
- Playlist:
  - added options to, open m3u files, add files and urls, sort, clear and save the playlist
  - added context menu for the playlist items
  - added setting to remember playlist state
  - removed rowHeight setting
  - fixed "Increase font size when fullscreen" setting not working
- Recent files: fixed ordering, showing duplicates, clearing and adding when list is full
- Added command line option to override the ytdl format selection setting (this option is overwritten if the GUI setting is changed)
- Added option to set a default cover for music files
- Added MPRIS thumbnail
- Added setting for subtitles to be rendered in black borders
- Fixed OSD showing without text

Issues should be reported here: https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic, but make sure to fill in the template and provide as much info as possible.
