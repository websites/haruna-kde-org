---
title: Haruna 1.2.0 - 1.2.1
description: Haruna 1.2.0 - 1.2.1
author: George Florea Bănuș
date: 2024-08-26T05:00:00+02:00
---

Haruna version 1.2.0 is out with a new footer style.

<video controls width="500">
  <source src="fs.webm" type="video/webm" />
</video>

----

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="70"></a>

Availability of other package formats depends on your distro and the people who package Haruna.

Windows version:
 - <a href="https://download.kde.org/stable/haruna/1.2.0/haruna-1.2.0-windows-gcc-x86_64.exe">haruna-1.2.0-windows-gcc-x86_64.exe</a>
 - <a href="https://download.kde.org/stable/haruna/1.2.0/haruna-1.2.0-windows-gcc-x86_64.7z">haruna-1.2.0-windows-gcc-x86_64.7z</a>
 - <a href="https://download.kde.org/stable/haruna/1.2.0/haruna-1.2.0-windows-gcc-x86_64-dbg.7z">haruna-1.2.0-windows-gcc-x86_64-dbg.7z</a>

**If you like Haruna then support its development: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)**

Feature requests and bugs should be posted on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but for bugs make sure to fill in the template and provide as much information as possible.

----

### Changelog:
#### 1.2.1
 * Fixed thumbnail preview popup not resizing after disabling the previews
 * Use the same volume component for both footer styles
#### 1.2.0
 * Added floating footer/bottom toolbar style with 2 ways to trigger it:
    * on every mouse movement of the video area
    * only when the mouse is in the lower part of the video area
 * Removed the docbook and moved its content to tooltips
 * Middle clicking the playlist scrolls to the playing item
