---
title: Haruna 0.11.0
description: Haruna 0.11.0
author: George Florea Bănuș
date: 2023-05-17T01:00:00+02:00
---

Haruna version 0.11.0 is out. You can get it now on flathub:

<a href="https://flathub.org/apps/details/org.kde.haruna"><img src="/assets/img/flathub-logo.png" alt="flathub logo" height="90"></a>

Availability of other package formats depends on your distro and the people who package Haruna.

💰 If you use the app and want it to improve make a donation: [GitHub Sponsors](https://github.com/sponsors/g-fb) | [Liberapay](https://liberapay.com/gfb/) | [PayPal](https://paypal.me/georgefloreabanus)

Issues should be reported on [bugs.kde.org](https://bugs.kde.org/enter_bug.cgi?product=Haruna&component=generic), but make sure to fill in the template and provide as much info as possible.

----

### Changelog:
#### 0.11.0
Features:
- Refactored the mpv code to improve performance
- When restoring from minimized state, playback state is set to what it was when the minimization happened (only applies if the "Pause on minimize" setting is turned on)
- Added Debug settings page, shows the paths to config files and allows to open them

Bugfixes:
- Fixed not being able to open files whose path contained certain characters
- Fixed not being able to open .ts and .rm files
- Fixed some text fields not saving their value under certain circumstances
- Fixed screenshot format setting not working
- Fixed recent files menu not closing
- Fixed playlist context menu not closing when playlist hides
